package com.banquito.core.settings.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "BNK_INFORMATION")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class BankInformation implements Serializable {

  private static final long serialVersionUID = 2651941551628L;

  @Id
  @Column(name = "BANK_CODE", length = 32, nullable = false)
  @NonNull
  @EqualsAndHashCode.Include
  private String bankCode;

  @Column(name = "NAME", length = 128, nullable = false)
  private String name;

  @Column(name = "CREATION_DATE", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;

  @Column(name = "TIMEZONE_ID", length = 32, nullable = false)
  private String timeZoneId;

  @Column(name = "COUNTRY", length = 3, nullable = false)
  private String country;

  @Column(name = "CURRENCY", length = 3, nullable = false)
  private String currency;

  @Column(name = "CITY", length = 64, nullable = false)
  private String city;

  @Column(name = "POSTAL_CODE", length = 8, nullable = true)
  private String postalCode;

  @Column(name = "WEBSITE", length = 128, nullable = true)
  private String webSite;

  @Column(name = "EMAIL", length = 128, nullable = true)
  private String email;

  @Column(name = "PHONE_NUMBER", length = 16, nullable = true)
  private String phoneNumber;

  @Column(name = "FISCAL_ID", length = 16, nullable = true)
  private String fiscalId;
}
