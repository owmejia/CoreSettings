package com.banquito.core.settings.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class BankInformationDTO {

    private String bankCode;

    private String name;

    private String country;

    private String currency;

    private String city;

    private String postalCode;

    private String webSite;

    private String email;

    private String phoneNumber;

    private String fiscalId;
}
