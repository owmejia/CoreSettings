package com.banquito.core.settings.service;

import com.banquito.core.settings.exception.BranchNotFoundException;
import com.banquito.core.settings.exception.RegionNotFoundException;
import com.banquito.core.settings.dao.BranchRepository;
import com.banquito.core.settings.dao.RegionRepository;
import com.banquito.core.settings.model.Branch;
import com.banquito.core.settings.model.Region;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BranchService {

  private final BranchRepository branchRepository;
  private final RegionRepository regionRepository;

  public BranchService(BranchRepository branchRepository, RegionRepository regionRepository) {
    this.branchRepository = branchRepository;
    this.regionRepository = regionRepository;
  }

  public Branch getByBranchId(String id) {
    Optional<Branch> branchOpt = this.branchRepository.findById(id);
    if (branchOpt.isPresent()) return branchOpt.get();
    else return null;
  }

  public Branch getByCode(String code) throws BranchNotFoundException {
    Optional<Branch> branchOpt = this.branchRepository.findByCode(code);
    if (branchOpt.isPresent()) return branchOpt.get();
    else throw new BranchNotFoundException("Branch Not Found with code " + code);
  }

  public List<Branch> getByRegion(String regionId) throws BranchNotFoundException {
   List<Branch> branches= this.branchRepository.findByRegionId(regionId);
    if (branches.isEmpty()){
      throw new BranchNotFoundException("Branches Not Found with region ");
    }
    return branches;
  }

  public Branch create(Branch branch) throws RegionNotFoundException, BranchNotFoundException {
    Optional<Branch> branchOpt = this.branchRepository.findById(branch.getId());
    if (branchOpt.isPresent()) {
      throw new BranchNotFoundException("Record already exist");
    }
    Optional<Region> regionOpt = this.regionRepository.findById(branch.getRegionId());
    if (regionOpt.isEmpty()){
      throw new RegionNotFoundException("Region not found");
    }
    return this.branchRepository.save(branch);
  }

  public Branch update(Branch branch) throws BranchNotFoundException {
    Optional<Branch> branchOpt = this.branchRepository.findById(branch.getId());
    if (branchOpt.isEmpty()){
      throw new BranchNotFoundException("Record not found");
    }
    Branch branchDB = branchOpt.get();
    branchDB.setCode(branch.getCode());
    branchDB.setName(branch.getName());
    branchDB.setAddressPostalCode(branch.getAddressPostalCode());
    branchDB.setPhoneNumber(branch.getPhoneNumber());
    branchDB.setLocationUrl(branch.getLocationUrl());
    branchDB.setEmail(branch.getEmail());
    return this.branchRepository.save(branchDB);
  }
}
