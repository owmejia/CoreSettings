package com.banquito.core.settings.dao;

import com.banquito.core.settings.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BranchRepository extends JpaRepository<Branch, String> {
  List<Branch> findByRegionId(String region);

  Optional<Branch> findByCode(String code);
}
