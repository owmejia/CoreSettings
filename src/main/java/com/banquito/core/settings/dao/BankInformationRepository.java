package com.banquito.core.settings.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.banquito.core.settings.model.BankInformation;

public interface BankInformationRepository extends JpaRepository<BankInformation, String> {}
