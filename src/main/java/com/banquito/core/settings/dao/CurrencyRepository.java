package com.banquito.core.settings.dao;

import com.banquito.core.settings.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyRepository extends JpaRepository<Currency, String> {}
