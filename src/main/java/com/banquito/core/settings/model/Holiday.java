package com.banquito.core.settings.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "BNK_HOLIDAY")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Holiday implements Serializable {

  private static final long serialVersionUID = 258771551628L;

  @Id
  @Column(name = "HOLIDAY_DATE", nullable = false)
  @Temporal(TemporalType.DATE)
  @NonNull
  @EqualsAndHashCode.Include
  private Date date;

  @Column(name = "NAME", length = 64, nullable = false)
  private String name;

  @Column(name = "REGION_ID", length = 32, nullable = false)
  private String regionId;

  @Column(name = "CREATION_DATE", nullable = false)
  @Temporal(TemporalType.TIMESTAMP)
  private Date creationDate;
}
