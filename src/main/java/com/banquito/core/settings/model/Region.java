package com.banquito.core.settings.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "BNK_REGION")
@Data
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Region implements Serializable {

  private static final long serialVersionUID = 78485941551628L;

  @Id
  @Column(name = "REGION_ID", length = 32, nullable = false)
  @NonNull
  @EqualsAndHashCode.Include
  private String id;

  @Column(name = "NAME", length = 64, nullable = false)
  private String name;

  @Column(name = "CODE", length = 16, nullable = false)
  private String code;
}
