package com.banquito.core.settings.service;

import com.banquito.core.settings.dao.CurrencyRepository;
import com.banquito.core.settings.exception.CurrencyNotFoundException;
import com.banquito.core.settings.model.Currency;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CurrencyService {

    private final CurrencyRepository currencyRepository;

    public Currency findById(String id) {

        Optional<Currency> currencyOpt = this.currencyRepository.findById(id);
        if (currencyOpt.isEmpty()) {
            return null;
        }
        return currencyOpt.get();
    }

    public Currency create(Currency currency) {
        Optional<Currency> currencyOpt = this.currencyRepository.findById(currency.getId());
        if (currencyOpt.isPresent()) {
            throw new CurrencyNotFoundException(
                    "Record Already exists"
            );
        }
        currency.setCreationDate(new Date());
        return this.currencyRepository.save(currency);
    }

    public Currency update(Currency currency) {
        Optional<Currency> currencyOpt = this.currencyRepository.findById(currency.getId());
        if (currencyOpt.isEmpty()) {
            throw new CurrencyNotFoundException(
                    "Record not found"
            );
        }
        Currency currencyDB = currencyOpt.get();
        currencyDB.setName(currency.getName());
        return this.currencyRepository.save(currencyDB);
    }

    public void delete(String id) {
        Optional<Currency> currencyOpt = this.currencyRepository.findById(id);
        if (currencyOpt.isEmpty()) {
            throw new CurrencyNotFoundException(
                    "Record not found"
            );
        }
        this.currencyRepository.delete(currencyOpt.get());
    }
}
