package com.banquito.core.settings.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class HolidayDTO {
    private Date date;

    private String name;

    private String regionId;
}
