package com.banquito.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileInputStream;
import java.io.IOException;

import lombok.extern.slf4j.Slf4j;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.*;

@Slf4j
@SpringBootApplication
public class SettingsApplication {
	private Storage storage;
	public static void main(String[] args) {
		// SettingsApplication googleCloudStorage = new SettingsApplication("./src/main/resources/prueba.json", "logs-357320");
		 
		log.info("Logging ERROR with LogbOmarack");
		log.info("Logging INFO with Logback");
  
		SpringApplication.run(SettingsApplication.class, args);
		log.info("Logging ERROR after with LogbOmarack");
		log.info("Logging INFO after with Logback");
	}

	/*private SettingsApplication(String pathToConfig, String projectId) throws IOException {
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(pathToConfig));
		storage = StorageOptions.newBuilder().setCredentials(credentials).setProjectId(projectId).build().getService();
    }*/
}

