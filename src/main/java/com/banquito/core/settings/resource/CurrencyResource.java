package com.banquito.core.settings.resource;

import com.banquito.core.settings.dto.CurrencyDTO;
import com.banquito.core.settings.model.Currency;
import com.banquito.core.settings.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path= "/currency")

public class CurrencyResource {

    private final CurrencyService currencyService;

    @GetMapping(path = "/{id}")
    public ResponseEntity<Currency> searchById(@PathVariable String id){
        return ResponseEntity.ok(this.currencyService.findById(id));
    }

    @PostMapping
    public ResponseEntity<CurrencyDTO> create(CurrencyDTO dto){
        try{
            Currency currency = this.currencyService.create(this.buildCurrency(dto));
            return ResponseEntity.ok(this.buildCurrencyDTO(currency));
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping
    public ResponseEntity<CurrencyDTO> update(CurrencyDTO dto){
        try{
            Currency currency = this.currencyService.update(this.buildCurrency(dto));
            return ResponseEntity.ok(this.buildCurrencyDTO(currency));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
    @DeleteMapping(path = "/{id}")
    public ResponseEntity delete(@PathVariable String id){
        try{
            this.currencyService.delete(id);
            return ResponseEntity.ok().build();
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }


    private Currency buildCurrency(CurrencyDTO dto){
        return Currency.builder()
                .name(dto.getName())
                .id(dto.getName()).build();
    }

    private CurrencyDTO buildCurrencyDTO(Currency currency){
        return CurrencyDTO.builder()
                .name(currency.getName())
                .id(currency.getId()).build();
    }
}
