package com.banquito.core.settings.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class HolidayException extends Exception {

  public HolidayException(String message) {
    super(message);
  }
}
