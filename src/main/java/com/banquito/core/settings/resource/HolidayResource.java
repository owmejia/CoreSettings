package com.banquito.core.settings.resource;

import com.banquito.core.settings.dto.HolidayDTO;
import com.banquito.core.settings.model.Holiday;
import com.banquito.core.settings.service.HolidayService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/holiday")
public class HolidayResource {

    private final HolidayService holidayService;

    @GetMapping(path = "/search/byDate")
    public ResponseEntity<Holiday> searchByDate(Date date){
        return ResponseEntity.ok(this.holidayService.findById(date));
    }

    @GetMapping(path = "/search/betweenDates")
    public ResponseEntity<List<Holiday>> searchBetweenDates(Date startDate, Date finalDate){
        return ResponseEntity.ok(this.holidayService.betweenDatesRegionNull(startDate, finalDate));
    }

    @GetMapping(path = "/search/beetweenDatesAndRegion")
    public ResponseEntity<List<Holiday>> searchBeetweenDatesAndRegion(Date startdate, Date finalDate, String region){
        return ResponseEntity.ok(
                this.holidayService.betweenDatesRegionNotNull(startdate, finalDate, region)
        );
    }

    @PostMapping
    public ResponseEntity<Holiday> createHoliday(HolidayDTO dto){
        Holiday holiday = this.buildHoliday(dto);
        try {
            this.holidayService.create(holiday);
            return ResponseEntity.ok().build();
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    private Holiday buildHoliday(HolidayDTO dto){
        return Holiday.builder()
                .date(dto.getDate())
                .name(dto.getName())
                .regionId(dto.getRegionId()).build();
    }

    private HolidayDTO buildHolidatDTO(Holiday holiday){
        return HolidayDTO.builder()
                .date(holiday.getDate())
                .name(holiday.getName())
                .regionId(holiday.getRegionId()).build();
    }
}

