package com.banquito.core.settings.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BranchDTO {
    private String id;

    private String code;

    private String name;

    private String addressCountry;

    private String addressCity;

    private String addressPostalCode;

    private String addressLine1;

    private String addressLine2;

    private String phoneNumber;

    private String locationUrl;

    private String email;

    private String regionId;

}
